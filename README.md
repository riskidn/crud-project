# Test Pemrograman Aplikasi (CRUD)

Adalah sebuah aplikasi CRUD atau Create, Read, Update, Delete. dengan menggunakan [React Js](https://github.com/facebook/create-react-app).

![React Version](https://img.shields.io/badge/react-18.2.0-green)
![Axios Version](https://img.shields.io/badge/axios-1.2.6-yellowgreen)
![Moment Version](https://img.shields.io/badge/moment-2.29.4-orange)
![React-bootstrap Version](https://img.shields.io/badge/react--bootstrap-2.7.0-blue)
![Sweetalert Version](https://img.shields.io/badge/sweetalert-2.1.2-important)

## Instalasi Dasar

Untuk langkah awal, kita install aplikasi react kita

```bash
    npx create-react-app crud-app
```

Setelah selesai, coba jalankan

### `npm start`

Server biasanya akan berjalan di [http://localhost:3000](http://localhost:3000)

## Instalasi Tambahan

lalu install beberapa framework dan library yang dibutuhkan

```bash
    npm install axios moment react-bootstrap sweetalert
```

**Axios** digunakan untuk merequest data api
**Moment** digunakan untuk memanipulasi tanggal di javascript
**React Bootstrap** digunakan untuk membantu pengembangan halaman terutama untuk frontend
**Seeetalert** digunakan sebagai pop up alert yang lebih cantik

Selain diatas, kita juga perlu install **react-router-dom** untuk proses routing

```bash
    npm install react-router-dom
```

## Backend Api

untuk backend api, gunakan [link backend](https://cms-admin.ihsansolusi.co.id/docs#/) sebagai server
