//=========================Soal no 1===========================
console.log("=====================Soal ke 1============================");
const getSentence = (value) => {
  let word = value
    .toLowerCase()
    .match(/[A-Za-z0-9 ]/g)
    .join("");

  //filter pertama
  let filter1 = word.split(" ");
  for (i = 0; i < filter1.length; i++) {
    filter1[i] = filter1[i][0].toUpperCase() + filter1[i].substr(1);
  }

  //filter kedua
  let filter2 = word.replace(/\s/g, "-");

  console.log(filter1.join(" "));
  console.log(filter2);
};

getSentence("SELamat Pagi Dunia!!");

//=========================Soal no 2===========================
console.log("=====================Soal ke 2============================");

const hitungHuruf = (str) => {
  var value = str.split("").sort();
  let hasil = [];
  let hitung = 1;
  for (i = 0; i < value.length; i++) {
    if (value[i] === value[i + 1]) {
      hitung++;
    } else {
      let huruf = ` ${value[i]} = ${hitung} \n`;
      hasil = [...hasil, huruf];
      hitung = 1;
    }
  }
  return hasil.join("");
};
console.log(hitungHuruf("aabbbahwws"));
//========================================Soal 3 =========================
console.log("=====================Soal ke 3============================");

const bilanganPangkat = (value) => {
  //1,2
  let result = [];
  for (i = 0; i < value; i++) {
    result.push(i);
  }
  //3
  let hasil = [];
  let angka = 0;
  let angka1 = 0;
  let angka2 = -1;

  for (j = 0; j < value; j++) {
    hasil.push(angka);
    angka = angka1 + angka2 + 1;
    angka2 = angka1;
    angka1 = angka;
  }

  console.log(result.map((x) => Math.pow(x + 1, 2)));
  console.log(result.map((x) => Math.pow(x, 2) + 1));
  console.log(hasil);
};
bilanganPangkat(10);
//========================================Soal 4 =========================
console.log("=====================Soal ke 4============================");

const cariNilai = (value) => {
  let result = value.split(",").map((x) => parseInt(x));
  let sum = result.reduce((a, b) => a + b);
  console.log("nilai terbesar adalah : ", Math.max(...result));
  console.log("nilai terkecil adalah : ", Math.min(...result));
  console.log("nilai rata-rata adalah : ", sum / result.length);
};
cariNilai("20,21,80,21,55,31,22");
