import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/esm/Button";
import { useState, useContext, useEffect } from "react";
import { UserContext } from "../context/UserContext";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import swal from "sweetalert";
import Placeholder from "react-bootstrap/Placeholder";
import edit from "../components/image/edit.svg";

const FormUser = () => {
  const {
    urlState: [url],
    userState: [user],
  } = useContext(UserContext);

  const { id } = useParams();
  const [loading, setLoading] = useState(true);
  const [message, setMessage] = useState("");
  const [input, setInput] = useState({
    id: null,
    name: "",
    address: "",
    gender: "",
    born_date: "",
  });
  const navigate = useNavigate();
  useEffect(() => {
    const fetchData = async () => {
      await axios
        .get(`${url}/testapi/user/${id}`, {
          headers: {
            Authorization: `Bearer ${user}`,
          },
        })
        .then((res) => {
          setInput(res.data.data);
          setLoading(false);
        })
        .catch((err) => {
          setLoading(false);
        });
    };
    fetchData();
  }, [id]);

  const handleChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    if (
      input.name === "" ||
      input.address === "" ||
      input.gender === "" ||
      input.born_date === ""
    ) {
      setLoading(false);
      setMessage("Maaf Masukan data dengan benar");
    } else if (input.name.length < 8) {
      setMessage("Input nama minimal 8 karakter atau lebih");
      setLoading(false);
    } else if (input.id === null) {
      try {
        await axios.post(
          `${url}/testapi/user`,
          {
            ...input,
          },
          {
            headers: { Authorization: `Bearer ${user}` },
          }
        );
        swal("Berhasil", "Berhasil Menambahkan Data", "success");
        setLoading(false);
        navigate("/list-all");
      } catch (err) {
        if (err) {
          swal("gagal", "Maaf, coba beberapa saat lagi", "warning");
          setLoading(false);
        }
      }
    } else {
      await axios
        .put(
          `${url}/testapi/user/${id}`,
          {
            ...input,
          },
          {
            headers: { Authorization: `Bearer ${user}` },
          }
        )
        .then((res) => {
          swal("Berhasil", "Berhasil Merubah Data", "success");
          setLoading(false);
          navigate("/list-all");
        })
        .catch((err) => {
          if (err) {
            setLoading(false);
            swal("gagal", "Maaf, coba beberapa saat lagi", "warning");
            console.log(err);
          }
        });
    }
  };

  return (
    <>
      <div className="login-page" style={{ flexDirection: "row-reverse" }}>
        <div className="column1">
          <center>
            <h2>Pastikan Inputan Sudah terisi</h2>
          </center>
          <button className="btn-login" onClick={() => navigate("/")}>
            Kembali ke menu utama 👈
          </button>
          <br />
          <button className="btn-login" onClick={() => navigate("/list-all")}>
            Kembali ke menu list 👈
          </button>
          <img
            width="400"
            src={edit}
            className="mt-3 mb-5 width"
            alt="login page"
          />
          <p> &copy;2023 Copyright RiskiDN</p>
        </div>
        <div className="column2">
          <center>
            <h3 className="text-primary fw-bold">Form User </h3>
          </center>
          <Form
            onSubmit={handleSubmit}
            style={{ border: "2px solid #ffff" }}
            className="form-padding"
          >
            {loading ? (
              <center>
                <Placeholder as="p" animation="glow">
                  <Placeholder xs={8} />
                </Placeholder>
              </center>
            ) : (
              <>
                <center>
                  <h4 className="margin mt-4 text-danger">{message}</h4>
                </center>
                <Form.Group className="margin" controlId="formBasicEmail">
                  <Form.Label className="fw-bold">Nama Lengkap</Form.Label>
                  <Form.Control
                    type="text"
                    name="name"
                    value={input.name}
                    onChange={handleChange}
                    placeholder="Masukan Nama"
                    minLength="8"
                    required
                  />
                </Form.Group>
                <Form.Group className="margin" controlId="formBasicPassword">
                  <Form.Label className="fw-bold">Alamat Lengkap</Form.Label>
                  <Form.Control
                    type="text"
                    name="address"
                    value={input.address}
                    onChange={handleChange}
                    as="textarea"
                    placeholder="Alamat Lengkap"
                    required
                  />
                </Form.Group>
                <Form.Group onChange={handleChange} className="margin">
                  <p className="fw-bold">Jenis Kelamin</p>
                  <Form.Check
                    inline
                    type="radio"
                    id="default-radio"
                    value="l"
                    label="Pria"
                    className="margin"
                    style={{ padding: "0 25px" }}
                    name="gender"
                    checked={input.gender === "l"}
                    readOnly
                  />
                  <Form.Check
                    inline
                    type="radio"
                    className="margin"
                    id="default-radio"
                    style={{ padding: "0 25px" }}
                    value="p"
                    label="Wanita"
                    name="gender"
                    checked={input.gender === "p"}
                    readOnly
                  />
                </Form.Group>
                <Form.Group className="margin">
                  <Form.Label className="fw-bold">Tanggal Lahir</Form.Label>
                  <Form.Control
                    type="date"
                    id="example-datepicker"
                    value={input.born_date}
                    name="born_date"
                    onChange={handleChange}
                    data-date-format="DDDD-MM-YYYY"
                    required
                  />
                </Form.Group>
                <center>
                  <Button variant="primary" type="submit">
                    Simpan
                  </Button>
                </center>
              </>
            )}
          </Form>
        </div>
      </div>
    </>
  );
};

export default FormUser;
