import React from "react";
import { Outlet } from "react-router-dom";

export const Base = () => {
  return (
    <div>
      <Outlet />
    </div>
  );
};
