import axios from "axios";
import { useContext, useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import { useNavigate, useParams } from "react-router-dom";
import { UserContext } from "../context/UserContext";
import moment from "moment";
import Placeholder from "react-bootstrap/Placeholder";
import lamp from "../components/image/lamp2.svg";

const ViewUser = () => {
  const {
    urlState: [url],
    userState: [user],
  } = useContext(UserContext);

  const { id } = useParams();
  const [profil, setProfil] = useState({});
  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get(`${url}/testapi/user/${id}`, {
        headers: {
          Authorization: `Bearer ${user}`,
        },
      })
      .then((res) => {
        setProfil(res.data.data);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  }, [id]);

  return (
    <>
      <div className="login-page" style={{ flexDirection: "row-reverse" }}>
        <div className="column1">
          <center>
            <h2>Pastikan Data Sudah sesuai</h2>
          </center>
          <button className="btn-login" onClick={() => navigate("/")}>
            Kembali ke menu utama 👈
          </button>
          <br />
          <button className="btn-login" onClick={() => navigate("/list-all")}>
            Kembali ke menu list 👈
          </button>
          <img
            width="400"
            src={lamp}
            className="mt-3 mb-5 width"
            alt="View page"
          />
          <p> &copy;2023 Copyright RiskiDN</p>
        </div>
        <div className="column2">
          <center>
            <h3 className="text-primary fw-bold">Detail User </h3>
            {loading ? (
              <center>
                <Placeholder as="p" animation="glow">
                  <Placeholder xs={8} />
                </Placeholder>
              </center>
            ) : (
              <>
                <div className="d-flex justify-content-center">
                  <div className="image">
                    <figure>
                      <img
                        alt=" profil pic"
                        src={profil.photo}
                        onError={(e) =>
                          (e.target.src =
                            "https://img.freepik.com/free-icon/user_318-790139.jpg?w=2000")
                        }
                      />
                    </figure>
                  </div>
                  <div className="info">
                    <p>
                      <b>Nama : </b>
                      {profil.name}
                    </p>
                    <p>
                      <b>Alamat : </b>
                      {profil.address}
                    </p>
                    <p>
                      <b>Tanggal Lahir : </b>{" "}
                      {moment(profil.born_date).format("DD MMM. YYYY")}
                    </p>
                    <p>
                      <b> Tanggal Input : </b>

                      {moment(profil.created_at).format("DD MMM YYYY hh:mm")}
                    </p>
                    <p>
                      <b>Gender : </b>
                      {profil.gender === "l" ? "Pria" : "Wanita"}
                    </p>
                    <Button
                      variant="outline-primary "
                      onClick={() => navigate(`/edit-user/${profil.id}`)}
                    >
                      Edit User
                    </Button>
                  </div>
                </div>
              </>
            )}
          </center>
        </div>
      </div>
    </>
  );
};

export default ViewUser;
