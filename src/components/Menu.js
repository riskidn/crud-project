import { Link, useNavigate } from "react-router-dom";
import React, { useContext } from "react";
import { UserContext } from "../context/UserContext";
import logo from "../components/image/void.svg";
import home from "../components/image/home.svg";
import Button from "react-bootstrap/esm/Button";
import img from "./image/img.svg";

const Menu = () => {
  const {
    userState: [user, setUser],
  } = useContext(UserContext);
  const navigate = useNavigate();

  const logout = () => {
    localStorage.clear();
    setUser(null);
  };

  return (
    <div
      className="home-page"
      style={{ display: "flex", flexWrap: "wrap", height: "100vh" }}
    >
      <div className="home1">
        <nav>
          <ul>
            <li style={{ marginBottom: "40px" }}>
              <center>
                <img src={logo} alt="" width="100" className="logo" />
              </center>
            </li>
            <li>
              <Link to="/list-all">List User</Link>
            </li>
            {user ? (
              <li>
                <Link to="/login" onClick={logout}>
                  Logout
                </Link>
              </li>
            ) : (
              <>
                {" "}
                <li>
                  <Link to="/login">Login</Link>
                </li>
                <li>
                  <Link to="/register">Register</Link>
                </li>
              </>
            )}
            <li style={{ marginTop: "40px" }}>
              <center>
                <img src={img} alt="" width="120" />
              </center>
            </li>
            <li>
              <center>
                <p className="mt-5 text-white"> &copy;2023 Copyright RiskiDN</p>
              </center>
            </li>
          </ul>
        </nav>
      </div>
      <div className="home2">
        <div className="margin center">
          <h1 className="text-primary">Selamat Datang </h1>

          {user ? (
            <center>
              <Button
                variant="outline-primary"
                className="mt-2 mb-2"
                onClick={() => navigate("/list-all")}
              >
                Daftar all list user
              </Button>
            </center>
          ) : (
            <h6 className="mt-3 mb-3">
              Pastikan Login terlebih dahulu untuk mengakses seluruh fitur
            </h6>
          )}

          <img width="600" src={home} alt="login page" className="width" />
        </div>
      </div>
    </div>
  );
};

export default Menu;
