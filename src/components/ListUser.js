import React, { useEffect, useContext, useState } from "react";
import axios from "axios";
import Table from "react-bootstrap/Table";
import { UserContext } from "../context/UserContext";
import Button from "react-bootstrap/esm/Button";
import { useNavigate, Link } from "react-router-dom";
import Spinner from "react-bootstrap/Spinner";
import swal from "sweetalert";
import moment from "moment";
import img from "./image/img.svg";
import logo from "../components/image/void.svg";

const ListUser = () => {
  const {
    urlState: [url],
    userState: [user],
  } = useContext(UserContext);

  const [userList, setUserList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [fetchTrigger, setFetchTrigger] = useState(true);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      let result = await axios.get(`${url}/testapi/user`, {
        headers: {
          Authorization: `Bearer ${user}`,
        },
      });
      setUserList(result.data.data);
      setFetchTrigger(false);
      setLoading(false);
    };

    if (fetchTrigger) {
      fetchData();
    }
  }, [fetchTrigger]);

  const handleDelete = (e) => {
    setLoading(true);
    let idUser = Number(e.target.value);
    axios
      .delete(`${url}/testapi/user/${idUser}`, {
        headers: {
          Authorization: `Bearer ${user}`,
        },
      })
      .then((res) => {
        setFetchTrigger(true);
        setLoading(false);
        swal("Congrats", "Berhasil Menghapus Data", "success");
      })
      .catch((err) => {
        setLoading(false);
        swal("Maaf", "Gagal menghapus data", "info");
      });
  };

  return (
    <>
      <div
        className="home-page"
        style={{ display: "flex", flexWrap: "wrap", height: "100vh" }}
      >
        <div className="home1">
          <nav>
            <ul>
              <li style={{ marginBottom: "40px" }}>
                <center>
                  <img src={logo} alt="" width="100" className="logo" />
                </center>
              </li>
              <li>
                <Link to="/">Menu Utama</Link>
              </li>
              <li style={{ marginTop: "40px" }}>
                <center>
                  <img src={img} alt="" width="120" />
                </center>
              </li>
              <li>
                <center>
                  <p className="mt-5 text-white">
                    {" "}
                    &copy;2023 Copyright RiskiDN
                  </p>
                </center>
              </li>
            </ul>
          </nav>
        </div>
        <div className="home2">
          <div className="container ">
            {loading ? (
              <center>
                <Spinner animation="border" variant="success" />{" "}
              </center>
            ) : (
              <>
                {" "}
                <center>
                  <h1 className="text-primary">List All User</h1>
                </center>
                <Button onClick={() => navigate("/add-user")} variant="success">
                  Tambah User Baru
                </Button>
                <div className="center">
                  {userList.length === 0 ? (
                    <h3>Data Masih Kosong</h3>
                  ) : (
                    <Table striped bordered hover className="mt-3 " responsive>
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama</th>
                          <th>Alamat</th>
                          <th>P/W</th>
                          <th>Tanggal Lahir</th>
                          <th>Tanggal Input</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        {userList?.map((item, index) => {
                          return (
                            <tr key={index}>
                              <td>{index + 1}</td>
                              <td>{item.name}</td>
                              <td>{item.address}</td>
                              <td>{item.gender === "l" ? "Pria" : "Wanita"}</td>
                              <td>
                                {moment(item.born_date)
                                  .utc()
                                  .format("DD MMM. YYYY")}
                              </td>
                              <td>
                                {moment(item.created_at).format(
                                  "DD MMM YYYY hh:mm"
                                )}
                              </td>
                              <td>
                                <Button
                                  className="m-2 mb-0 mt-0"
                                  onClick={() =>
                                    navigate(`/view-user/${item.id}`)
                                  }
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="16"
                                    height="16"
                                    fill="currentColor"
                                    className="bi bi-eye-fill"
                                    viewBox="0 0 16 16"
                                  >
                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
                                    <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z" />
                                  </svg>
                                </Button>
                                <Button
                                  className="m-2 mb-0 mt-0"
                                  onClick={() =>
                                    navigate(`/edit-user/${item.id}`)
                                  }
                                  variant="success"
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="16"
                                    height="16"
                                    fill="currentColor"
                                    className="bi bi-pencil-fill"
                                    viewBox="0 0 16 16"
                                  >
                                    <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z" />
                                  </svg>
                                </Button>
                                <Button
                                  className="m-2 mb-0 mt-0"
                                  value={item.id}
                                  onClick={handleDelete}
                                  variant="danger"
                                >
                                  X
                                </Button>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </Table>
                  )}
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default ListUser;
