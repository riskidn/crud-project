import { useContext } from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { Base } from "../components/Base";
import FormUser from "../components/FormUser";
import ViewUser from "../components/ViewUser";
import Login from "../utils/Login";
import Register from "../utils/Register";
import { UserContext } from "../context/UserContext";
import Menu from "../components/Menu";
import ListUser from "../components/ListUser";

const NotFound = () => {
  return <h1>Halaman Tidak ditemukan</h1>;
};

const MainRoute = () => {
  const {
    userState: [user],
  } = useContext(UserContext);

  const LoginRoute = ({ user, children }) => {
    if (user) {
      return <Navigate to="/" />;
    }
    return children;
  };
  const ProtectedRoute = ({ user, children }) => {
    if (!user) {
      return <Navigate to="/login" />;
    }
    return children;
  };
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Base />}>
          <Route index element={<Menu />} />
          <Route
            path="/list-all"
            element={
              <ProtectedRoute user={user}>
                <ListUser />
              </ProtectedRoute>
            }
          />
          <Route
            path="/add-user"
            element={
              <ProtectedRoute user={user}>
                <FormUser />
              </ProtectedRoute>
            }
          />
          <Route
            path="/edit-user/:id"
            element={
              <ProtectedRoute user={user}>
                <FormUser />
              </ProtectedRoute>
            }
          />
          <Route
            path="/view-user/:id"
            element={
              <ProtectedRoute user={user}>
                <ViewUser />
              </ProtectedRoute>
            }
          />
          <Route
            path="/login"
            element={
              <LoginRoute user={user}>
                <Login />
              </LoginRoute>
            }
          />
          <Route
            path="/register"
            element={
              <LoginRoute user={user}>
                <Register />
              </LoginRoute>
            }
          />
          <Route path="*" element={<NotFound />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};
export default MainRoute;
