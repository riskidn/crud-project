import "./App.css";
import MainRoute from "./routes";
import "bootstrap/dist/css/bootstrap.min.css";
import { UserProvider } from "./context/UserContext";
import "./components/css/style.css";

function App() {
  return (
    <UserProvider>
      <MainRoute />
    </UserProvider>
  );
}

export default App;
