import React, { createContext, useState } from "react";

export const UserContext = createContext();
export const UserProvider = ({ children }) => {
  const base_url = "https://cms-admin.ihsansolusi.co.id";
  const [url] = useState(base_url);
  const [user, setUser] = useState(JSON.parse(localStorage.getItem("user")));

  const state = {
    urlState: [url],
    userState: [user, setUser],
  };

  return <UserContext.Provider value={state}>{children}</UserContext.Provider>;
};
