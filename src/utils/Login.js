import axios from "axios";
import React, { useState, useContext } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { Link, useNavigate } from "react-router-dom";
import { UserContext } from "../context/UserContext";
import login from "../components/image/adventure.svg";
import swal from "sweetalert";
import Placeholder from "react-bootstrap/Placeholder";

const Login = () => {
  let initial = {
    email: "nugrohoriski157@gmail.com",
    password: "password",
  };
  const navigate = useNavigate();
  const {
    urlState: [url],
    userState: [, setUser],
  } = useContext(UserContext);
  const [account, setAccount] = useState(initial);
  const [message, setMessage] = useState("");
  const [loading, setLoading] = useState(false);

  const handleChange = (e) => {
    setAccount({ ...account, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    if (account.email === "" || account.password === "") {
      setMessage("Masukan Data Dengan Lengkap");
      setLoading(false);
    } else {
      await axios
        .post(`${url}/testapi/auth/login`, {
          email: account.email,
          password: account.password,
        })
        .then((response) => {
          let token = response.data.token;
          setUser(token);
          localStorage.setItem("user", JSON.stringify(token));
          swal("Good job!", response.data.detail, "success");
          setLoading(false);
          navigate("/");
        })
        .catch((err) => {
          if (err) {
            setLoading(false);
            setMessage(err.response.data.detail);
          }
        });
    }
  };

  return (
    <>
      <div className="login-page">
        <div className="column1">
          <center>
            <h2>Hallo, Silakan Login Terlebih Dahulu</h2>
          </center>
          <button className="btn-login" onClick={() => navigate("/")}>
            Kembali ke menu utama 👈
          </button>
          <img
            width="400"
            src={login}
            className="mt-3 mb-5 width"
            alt="login page"
          />
          <p> &copy;2023 Copyright RiskiDN</p>
        </div>
        <div className="column2">
          <center>
            <h3>Login Page</h3>
          </center>
          <Form
            onSubmit={handleSubmit}
            style={{ border: "2px solid #ffff" }}
            className="form-padding"
          >
            {loading ? (
              <center>
                <Placeholder as="p" animation="glow">
                  <Placeholder xs={8} />
                </Placeholder>
              </center>
            ) : (
              <center>
                <h4 className="margin mt-4 text-danger">{message}</h4>
              </center>
            )}
            <Form.Group className="margin" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                name="email"
                value={account.email}
                type="email"
                placeholder="default : nugrohoriski157@gmail.com"
                onChange={handleChange}
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>
            <Form.Group className="margin" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                name="password"
                type="password"
                value={account.password}
                placeholder="default : password"
                minLength="8"
                onChange={handleChange}
                onKeyDown={(e) => {
                  if (e.key === "Enter") {
                    handleSubmit();
                  }
                }}
              />
            </Form.Group>
            <Form.Group className="margin">
              <Button variant="primary" type="submit" id="myBtn">
                Submit
              </Button>
            </Form.Group>
          </Form>
          <center>
            <Link to="/register">Daftar akun</Link>
          </center>
        </div>
      </div>
    </>
  );
};

export default Login;
